import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { fetchElevatorById } from "../../store/elevatorDetails/actions";
import { selectElevatorDetails } from "../../store/elevatorDetails/selectors";
import { changeFloor } from "../../store/elevatorDetails/actions";
import { selectToken } from "../../store/user/selectors";
import { Link } from "react-router-dom";

import { Container } from "react-bootstrap";
import { Jumbotron } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Form } from "react-bootstrap";

export default function ArtworksDetails() {
  const { id } = useParams();
  const elevator = useSelector(selectElevatorDetails);
  const dispatch = useDispatch();
  const token = useSelector(selectToken);
  const [newFloor, setNewFloor] = useState(0);

  useEffect(() => {
    dispatch(fetchElevatorById(id));
  }, [dispatch, id]);


  const submitForm = (newFloor, id) => {
    dispatch(changeFloor(newFloor, id));
  };

  return (
    <div>
      <Jumbotron>
        <h1>{elevator.name}</h1>
        <h6>Current floor {elevator.curr_floor}</h6>
      </Jumbotron>
      <Container>
        <Row className="mb-5" className="text-center">
          {token ? (
            <Form
              inline
              as={Row}
              className="p-4 m-0 text-center"
              style={{ display: "inline-flex" }}
            >
              <Form.Group>
                <Form.Label className="mr-2">Pick the floor</Form.Label>
                <Form.Control
                  className="mr-2"
                  type="number"
                  min="-1" 
                  max="15"
                  value={newFloor}
                  onChange={(event) => setNewFloor(event.target.value)}
                  required
                />
              </Form.Group>
              <Button
                variant="success"
                type="submit"
                onClick={() => submitForm(newFloor, id)}
              >
                Change the floor
              </Button>
            </Form>
          ) : (
            <div className="mt-4">
              <h5>
                <Link to={`/signup`}>SignUp</Link> or{" "}
                <Link to={`/login`}>Login</Link> in order to bid
              </h5>
            </div>
          )}
        </Row>
      </Container>
    </div>
  );
}
