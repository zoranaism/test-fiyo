import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchElevators } from "../../store/elevators/actions";
import { selectElevators } from "../../store/elevators/selectors";
import { useParams } from "react-router-dom";
import { postElevator } from "../../store/elevators/actions";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import { Jumbotron } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { Link } from "react-router-dom";

// import ElevatorCard from "../../components/ElevatorCard";

export default function Elevators() {
  const dispatch = useDispatch();
  const elevators = useSelector(selectElevators);
  const [name, setName] = useState("");
  const [curr_floor, setCurr_floor] = useState("");
  const { id } = useParams();

  // console.log("ELEVATORS HOME PGAE", elevators);

  useEffect(() => {
    dispatch(fetchElevators());
  }, [dispatch]);

  function submitElevator(event) {
    event.preventDefault();
    // console.log(name, curr_floor);
    dispatch(postElevator(name, curr_floor));
    setName("");
    setCurr_floor("");
  }

  return (
    <div>
      <Jumbotron>
        <h1>Elevators</h1>
      </Jumbotron>
      <Container>
        {elevators.map((elevator) => {
          return (
            <div style={{ display: "block" }} key={elevator.id}>
              <Row>
                <Link
                  to={`/elevators/${elevator.id}`}
                >
                  Elevator {elevator.name} is currently on the floor {elevator.curr_floor}.
                </Link>
                <br />
              </Row>
            </div>
          );
        })}
        <Row>
          <h2>Create new elevator</h2>
        </Row>
        <Row>
          <Form>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={name}
                onChange={(event) => setName(event.target.value)}
                type="text"
                placeholder="Name of your elevator"
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Current floor</Form.Label>
              <Form.Control
                value={curr_floor}
                onChange={(event) => setCurr_floor(event.target.value)}
                type="number"
                required
              />
            </Form.Group>

            <Form.Group className="mt-5">
              <Button variant="success" type="submit" onClick={submitElevator}>
                Create elevator
              </Button>
            </Form.Group>
          </Form>
        </Row>
      </Container>
    </div>
  );
}
