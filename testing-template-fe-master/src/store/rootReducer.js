import { combineReducers } from "redux";
import appState from "./appState/reducer";
import user from "./user/reducer";
import elevators from "./elevators/reducer";
import elevatorDetails from "./elevatorDetails/reducer";

export default combineReducers({
  appState,
  user, 
  elevators, 
  elevatorDetails
});
