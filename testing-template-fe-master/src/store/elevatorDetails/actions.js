import axios from "axios";
import { apiUrl } from "../../config/constants";
import {
  appLoading,
  appDoneLoading,
  showMessageWithTimeout,
  setMessage,
} from "../appState/actions";
import { selectToken } from "../user/selectors";

export const ELEVATOR_DETAILS_FETCHED = "ELEVATOR_DETAILS_FETCHED";
export const ELEVATOR_FLOOR_CHANGED = "ELEVATOR_FLOOR_CHANGED";

const elevatorDetailsFetched = (elevator) => ({
  type: ELEVATOR_DETAILS_FETCHED,
  payload: elevator,
});

const elevatorFloorChanged = (elevatorNewFloor) => ({
  type: ELEVATOR_FLOOR_CHANGED,
  payload: elevatorNewFloor,
});

export const fetchElevatorById = (id) => {
  return async (dispatch, getState) => {
    dispatch(appLoading());
    try {
      const response = await axios.get(`${apiUrl}/elevators/${id}`);

      dispatch(elevatorDetailsFetched(response.data));
      dispatch(appDoneLoading());
    } catch (error) {
      if (error.response) {
        console.log(error.response.message);
        dispatch(setMessage("danger", true, error.response.data.message));
      } else {
        console.log(error);
        dispatch(setMessage("danger", true, error.message));
      }
      dispatch(appDoneLoading());
    }
  };
};

export const changeFloor = (newFloor, elevatorId) => {
  return async (dispatch, getState) => {
    try {
      const response = await axios.patch(`${apiUrl}/elevators/${elevatorId}`, {
        newFloor,
      });
      // console.log(
      //   "this is the response in the thunk elevator floor changed",
      //   response.data.newFloor
      // );
      dispatch(
        showMessageWithTimeout("success", false, response.data.message, 3000)
      );
      dispatch(elevatorFloorChanged(response.data.newFloor));
    } catch (error) {
      if (error.response) {
        console.log(error.response.message);
        dispatch(setMessage("danger", true, error.response.data.message));
      } else {
        console.log(error);
        dispatch(setMessage("danger", true, error.message));
      }
    }
  };
};
