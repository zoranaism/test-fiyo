import { ELEVATOR_DETAILS_FETCHED } from "./actions";
import { ELEVATOR_FLOOR_CHANGED } from "./actions";

const initialState = {
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ELEVATOR_DETAILS_FETCHED:
      return { ...state, ...action.payload };

    case ELEVATOR_FLOOR_CHANGED:
      return {
        ...state,
        curr_floor: action.payload,
      };

    default:
      return state;
  }
};
