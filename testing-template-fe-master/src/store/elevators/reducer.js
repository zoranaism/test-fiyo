import { FETCH_ELEVATORS_SUCCESS } from "./actions";
import { CREATE_ELEVATOR_SUCCESS } from "./actions";

const initialState = [

];

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ELEVATORS_SUCCESS:
      return [...action.payload];

    case CREATE_ELEVATOR_SUCCESS:
      return [ ...state, action.payload];

    default:
      return state;
  }
};