import { apiUrl } from "../../config/constants";
import axios from "axios";
import {
  appLoading,
  appDoneLoading,
  showMessageWithTimeout,
  setMessage,
} from "../appState/actions";
import { selectToken } from "../user/selectors";

export const FETCH_ELEVATORS_SUCCESS = "FETCH_ELEVATORS_SUCCESS";
export const CREATE_ELEVATOR_SUCCESS = "CREATE_ELEVATOR_SUCCESS";

export const fetchElevatorsSuccess = (elevators) => ({
  type: FETCH_ELEVATORS_SUCCESS,
  payload: elevators,
});

export const postElevatorsSuccess = (newElevator) => ({
  type: CREATE_ELEVATOR_SUCCESS,
  payload: newElevator,
});

export const fetchElevators = () => {
  return async (dispatch, getState) => {
    dispatch(appLoading());

    try {
      const response = await axios.get(`${apiUrl}/elevators`);

      // console.log(response.data);
      dispatch(fetchElevatorsSuccess(response.data));
      dispatch(appDoneLoading());
    } catch (error) {
      if (error.response) {
        dispatch(setMessage("danger", true, error.response.data.message));
      } else {
        dispatch(setMessage("danger", true, error.message));
      }
      dispatch(appDoneLoading());
    }
  };
};

export const postElevator = (name, curr_floor) => {
  return async (dispatch, getState) => {
    try {
      const token = selectToken(getState());
      console.log(name, curr_floor);

      const response = await axios.post(
        `${apiUrl}/elevators`,
        {
          name,
          curr_floor,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      console.log("Yep!", response.data);

      dispatch(
        showMessageWithTimeout("success", false, response.data.message, 3000)
      );
      dispatch(postElevatorsSuccess(response.data));
    } catch (error) {
      if (error.response) {
        dispatch(setMessage("danger", true, error.response.data.message));
      } else {
        dispatch(setMessage("danger", true, error.message));
      }
    }
  };
};
