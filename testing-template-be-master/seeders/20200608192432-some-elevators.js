'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "elevators",
      [
        {
          name: "Pekelharing",
          curr_floor: 3, 
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Smallegange",
          curr_floor: 5, 
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("elevators", null, {});
  },
};
