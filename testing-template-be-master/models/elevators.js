'use strict';
module.exports = (sequelize, DataTypes) => {
  const elevators = sequelize.define('elevators', {
    name:  { type: DataTypes.STRING, unique: true },
    curr_floor: DataTypes.INTEGER
  }, {});
  elevators.associate = function(models) {
    // associations can be defined here
  };
  return elevators;
};