const { Router } = require("express");
const User = require("../models").user;
const Elevator = require("../models").elevators;
const auth = require("../auth/middleware");
const Sequelize = require("sequelize");

const router = new Router();

router.get("/", async (req, res, next) => {
  try {
    const allElevators = await Elevator.findAll({
      order: [[Sequelize.literal("curr_floor"), "DESC"]],
    });

    res.status(200).json(allElevators);
  } catch (e) {
    next(e);
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    const id = parseInt(req.params.id);

    const elevator = await Elevator.findByPk(id);

    if (!elevator) {
      return res.status(404).send({ message: "Elevator not found" });
    }

    res.status(200).json(elevator);
  } catch (e) {
    next(e);
  }
});

router.patch("/:id", async (req, res, next) => {
  const { newFloor } = req.body;

  try {
    const elevatorId = parseInt(req.params.id);

    const elevator = await Elevator.findByPk(elevatorId);

    if (!elevator) {
      return res.status(404).send({ message: "Elevator not found" });
    }

    elevator.update({ curr_floor: newFloor });

    return res
      .status(200)
      .json({
        message: `You have successfully reached floor ${newFloor}`,
        newFloor,
      });
  } catch (e) {
    next(e);
  }
});

router.post("/", auth, async (req, res, next) => {
  const { name, curr_floor } = req.body;

  if (!curr_floor || !name) {
    return res.status(400).send({
      message: "Please provide a name and current floor new elevator is on.",
    });
  }

  try {
    userId = req.user.id;

    const user = await User.findByPk(userId, {
      raw: true,
    });

    if (!user) {
      return res
        .status(403)
        .send({ message: "You are not authorized to create new elevator." });
    }

    const newElevator = await Elevator.create({
      name,
      curr_floor,
    });

    return res.status(201).send({
      message: "Elevator successfully created! Have fun changing floors.",
      newElevator,
    });
  } catch (e) {
    next(e);
  }
});

module.exports = router;
